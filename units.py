import pygame
from config import *
from maps import Tile

class Unit(pygame.sprite.Sprite):
    MAX_DISTANCE = 0
    texture1 = pygame.image.load('missing_texture.png')
    texture2 = pygame.image.load('missing_texture.png')
    def __init__(self, x, y):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.Surface((tile_side, tile_side))
        self.image.blit(self.texture1, (0, 0))
        self.rect = pygame.Rect(x*tile_side, y*tile_side, tile_side, tile_side)
        self.select = False
        self.border_zone = ZoneToGo(self.MAX_DISTANCE)
        
    def draw(self, screen):
        screen.blit(self.image, (self.rect.x, self.rect.y))
        pygame.display.update()

    def canGoHere(self, map_, x, y):
        if not map_[x//tile_side][y//tile_side].tileName in cant_go:
            return True
        return False

    def canReachIt(self, x, y):
        if abs(x - self.rect.x)//tile_side + abs(y - self.rect.y)//tile_side <= self.MAX_DISTANCE:
            return True
        return False
    
    def warp(self, pos, screen, map_):
        new_x, new_y = pos[0]//tile_side*tile_side, pos[1]//tile_side*tile_side
        if self.canReachIt(new_x, new_y) and self.canGoHere(map_, new_x, new_y):
            self.rect.x = new_x
            self.rect.y = new_y
        
    def selectement(self, pos, screen, map_):
        if not self.select:
            if pos[0] > self.rect.x and pos[0] < self.rect.x + tile_side and pos[1] > self.rect.y and pos[1] < self.rect.y + tile_side:
                self.select = True
                self.image.blit(self.texture2, (0, 0))
                self.border_zone.showZone(self.rect.x, self.rect.y, screen)
        else:
            self.image.blit(self.texture1,  (0, 0))
            self.warp(pos, screen, map_.array)
            self.select = False
        self.draw(screen)
    
class Scout(Unit):
    MAX_DISTANCE = 5
    texture1 = pygame.image.load('scout.png')
    texture2 = pygame.image.load('scout_pressed.png')
    def __init__(self, x, y):
        Unit.__init__(self, x, y)

class Settler(Unit):
    MAX_DISTANCE = 2
    texture1 = pygame.image.load('settler.png')
    texture2 = pygame.image.load('settler_pressed.png')
    def __init__(self, x, y):
        Unit.__init__(self, x, y)

class ZoneToGo(): ##Zone you can go
    zone_side = 14
    dxy = (tile_side-zone_side)//2

    def __init__(self, radius):
        self.borders = self.genZone(radius)

    def genZone(self, radius):
        borders = []
        for i in range(radius*2+1):
            for j in range(radius*2+1):
                if abs(i - radius) + abs(j - radius) == radius:
                    borders.append([Tile('border', '#ffff00', self.zone_side), (i - radius)*tile_side, (j - radius)*tile_side]) ##Tile, dx, dy
        return borders

    def showZone(self, x, y, screen):
        for tile in self.borders:
            screen.blit(tile[0].surface, (x + tile[1] + self.dxy, y + tile[2] + self.dxy))
