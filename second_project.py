import pygame
from config import *
from units import *
from maps import *
pygame.init()

def set_screen():
    screen = pygame.display.set_mode((width_pix,height_pix))                      
    pygame.display.set_caption("Civ")
    icon = pygame.Surface((tile_side, tile_side))
    icon.blit(pygame.image.load('scout_pressed.png'), (0, 0))
    pygame.display.set_icon(icon)
    return screen
    
def main():
    screen = set_screen()
    map_ = Map(map_types)
    units = [Scout(5, 5), Scout(2, 3), Scout(12, 9), Scout(7, 15), Settler(3, 2), Settler(15, 3), Settler(10, 8)]
    in_loop = True
    map_.updateMap(screen)
    [i.draw(screen) for i in units]
    while in_loop:
        for event in pygame.event.get():
            if event.type == pygame.MOUSEBUTTONDOWN and pygame.mouse.get_pressed() == (1, 0, 0):
                map_.updateMap(screen)
                [i.selectement(event.pos, screen, map_) for i in units]
            if event.type == pygame.QUIT:
                pygame.display.quit()
                in_loop = False

main()
        

