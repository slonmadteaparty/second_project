height_pix = 900
width_pix = 900
tiles_height = 30
tile_side = height_pix // tiles_height
map_types = {
    (1,30):{'forest':'#00aa00'},
    (30,35):{'mountains':'#9d9d9d'},
    (35,80):{'ground':'#00dd00'},
    (80,100):{'water':'#0000aa'},
}

cant_go = ['water', 'mountains']
