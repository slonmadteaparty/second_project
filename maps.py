import pygame
import random
from config import *

class Tile():
    def __init__(self, tileName, tileType, side=tile_side):
        self.tileName = tileName
        self.tileType = tileType
        self.surface = pygame.Surface((side, side))
        self.surface.fill(pygame.Color(tileType))

class Map():
    height, width = 30, 30
    def __init__(self, map_types):
        self.array = self.createRandomMap(map_types)
        

    def createRandomMap(self, map_types):
        map_ = []
        for i in range(self.width):
            map_.append([])
            for j in range(self.height):
                dice = random.randint(1,100)
                
                for (min_range, max_range), tile in map_types.iteritems():
                    if min_range <= dice <= max_range:
                        map_[i].append(Tile(tile.items()[0][0], tile.items()[0][1]))
                        continue
        return map_

    def updateMap(self, screen):
        for i in range(self.width):
            for j in range(self.height):
                screen.blit(self.array[i][j].surface,(i*tile_side, j*tile_side))
        pygame.display.update()
